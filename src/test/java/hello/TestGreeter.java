package hello;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

// Change the comment again to kick off a build

public class TestGreeter {

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty()

   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }



   @Test
   @DisplayName("Test for Name='Tyler'")
   public void testGreeter()
   {

      g.setName("Tyler");
      assertEquals(g.getName(),"Tyler");
      assertEquals(g.sayHello(),"Hello Tyler!");
   }

   @Test
   @DisplayName("assertFalse Test for Name='secret'")
   public void testFalseSecret()
   {

      g.setName("Tyler");
      assertFalse(g.getName() == "secret","");
      assertFalse(g.getName() == "secret","Hello secret!");
   }

}
